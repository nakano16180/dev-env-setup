#!/bin/bash

# Install dependencies
echo "ユーザのパスワード" | sudo apt update && sudo apt install -y --no-install-recommends \
        make \
        build-essential \
        libssl-dev \
        zlib1g-dev \
        libbz2-dev \
        libreadline-dev \
        libsqlite3-dev \
        wget \
        curl \
        llvm \
        libncurses5-dev \
        xz-utils \
        tk-dev \
        libxml2-dev \
        libxmlsec1-dev \
        libffi-dev \
        liblzma-dev \
        git

# Download pyenv
git clone https://github.com/pyenv/pyenv.git ~/.pyenv

# Update .bashrc
echo -e "# pyenv paths" >> ~/.bashrc
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bashrc

exec "$SHELL"
pyenv -v

# Install Python and set default
pyenv install 3.7.4
pyenv global 3.7.4

python3 -m pip install --user pipx
python3 -m pipx ensurepath

pipx install pipenv