## Nodejs

### nvm、nodejs
以下のコマンドでnvmをインストール。

```
$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
```

使用するNode.jsのバージョンを確認しインストール。
```
$ nvm install --lts --latest-npm
$ nvm alias default lts/*
```

### yarn

```
$ curl --compressed -o- -L https://yarnpkg.com/install.sh | bash
```