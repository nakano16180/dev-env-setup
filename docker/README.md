## Docker & Docker compose

[Docker ドキュメント日本語化プロジェクト — Docker-docs-ja 19.03 ドキュメント](https://docs.docker.jp/index.html)

### 準備
まずは依存ライブラリのインストール

```
$ sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
```

aptのkeyを追加

```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable"
```

### Docker本体のインストール
dockerをインストール

```
$ sudo apt update
$ sudo apt install -y docker-ce
```

dockerの起動確認

```
$ sudo systemctl status docker
$ sudo docker ps
```

### 追加設定
sudo なしで実行出来るように設定

```
$ whoami
$ docker ps
$ cat /etc/group | grep docker
$ sudo gpasswd -a kaito docker
$ cat /etc/group | grep docker
$ sudo chmod 666 /var/run/docker.sock
$ docker ps
```

### Docker compose

```
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

$ docker-compose --version
```
