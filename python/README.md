## Python 

### setup on Ubuntu18.04

```
$ sudo apt install python3-pip python3-venv
```

### pipx

```
$ python3 -m pip install --user pipx 
```

```
$ python3 -m pipx ensurepath 
```

### pipenv

```
$ pipx install pipenv 
```