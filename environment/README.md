## Ubuntu

### [direnv](https://github.com/direnv/direnv)

direnvをインストール
```
$ curl -sfL https://direnv.net/install.sh | bash
```

~/.bashrcに以下を追加
```
export EDITOR=gedit
eval "$(direnv hook bash)"
```

### direnvとpython

- [direnvでpyenvやpipenvを使う - keyの試したこと](https://key3tech.hateblo.jp/entry/2021/01/18/210000)

```
$ pipenv install --python=3.7
$ echo 'layout pipenv' > .envrc && direnv allow
```

### direnvとnodejs
自分の作業用ディレクトリに移動し.nvmrcファイルを作成
自分の使いたいnodejsのバージョンを.nvmrcファイルに記述

```
$ cd ~/workspase/
$ touch .nvmrc
```
以下のコマンドで.envrcファイルを作成
```
$ direnv edit .
```

.envrcファイルに以下の内容を記述する
```
nvmrc=~/.nvm/nvm.sh
source $nvmrc
nvm use
```