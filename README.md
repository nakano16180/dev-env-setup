## Environment setup for development

### ターミナル設定

```
$ ./setup.sh
```

- bash_historyに実行日時を記録
- [starship](https://starship.rs/ja-jp/guide/)

### Python

```
$ ./install.sh
```
- pyenv
- pipx
- pipenv

### Nodejs
- nvm
- node
- yarn

### その他

- fzf
  - https://github.com/junegunn/fzf
    ```
    $ git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    $ ~/.fzf/install
    ```
  - [fzfを活用してTerminalの作業効率を高める - Qiita ](https://qiita.com/kamykn/items/aa9920f07487559c0c7e)
  - [fzfを使おう - Qiita ](https://qiita.com/kompiro/items/a09c0b44e7c741724c80)

- VScode
  - https://code.visualstudio.com/
  - [VSCode debパッケージでアップデート | infraya.work ](https://infraya.work/posts/update_vscode_with_deb_package/)


- 画面録画 (Simple Screen Recorder)
- Chrome

- Docker & Docker-compose
